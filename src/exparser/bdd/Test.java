package exparser.bdd;

import java.io.File;
import java.io.FileReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;

import net.sf.javabdd.BDD;
import exparser.bdd.tools.BDDUtils;
import exparser.bdd.tools.BDDUtils.BDDAllsatIterator;
import exparser.bdd.tools.BddManager;

public class Test {
	public static void showSolutions(BDD model) {
		if (model.equals(BddManager.falseFormula)) {
			System.out.println("infeasible");
		} else {
			// print all solutions
			System.out.println("List of abstract solutions:");
			System.out.println("(An abstract solution might contain dont care variables, denoted by [var], and represent multiple conrecte solutions.)");
			BDDAllsatIterator iter = new BDDAllsatIterator(model);
			Collection<BDD> varList = BddManager.varToBDD.values();
			int solution_count=0;
			for (Object sol : iter) {
				byte[] solution = (byte[]) sol;
				if (solution!=null) {
					solution_count++;
					ArrayList<String> solutionString_list = new ArrayList<String>();
					for (BDD varBDD : varList) {
						if (solution[varBDD.var()] == 1)
							solutionString_list.add(BddManager.BDDToString(varBDD, false, false));
						else if (solution[varBDD.var()] == 0)
							solutionString_list.add("!" + BddManager.BDDToString(varBDD, false, false));
						else if (solution[varBDD.var()] == -1)
							solutionString_list.add("[" + BddManager.BDDToString(varBDD, false, false) + "]");
					}
					System.out.println(BDDUtils.arrayToString(solutionString_list.toArray()));
				}
			}
			System.out.println("found " + solution_count + " solutions");
		}
	}
	public static void main(String[] args) throws Exception {
		File inFile = new File("./Formula.txt");
		ExparserBddParser p = new ExparserBddParser(ScannerCreator.createScanner(new FileReader(inFile)));
		
		// parsing happens here
		System.out.println("parsing " + inFile.getAbsolutePath());
		BDD model = (BDD) p.parse().value;
		// evaluation of the model
		showSolutions(model);
	}
}

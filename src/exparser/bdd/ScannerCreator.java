package exparser.bdd;

import java.io.InputStream;
import java.io.Reader;

/**
 * These functions provide public access to the package-private constructor of ExparserBDDScanner.
 * We cant just change the constructor modifier as it will be overwritten when the scanner is re-generated.
 */
public class ScannerCreator {
	public static ExparserBddScanner createScanner (Reader in) {
	    return new ExparserBddScanner(in);
	  }
	public static ExparserBddScanner createScanner (InputStream in) {
	    return new ExparserBddScanner(in);
	  }
}

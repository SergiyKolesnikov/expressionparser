
package exparser.sat;

import java_cup.runtime.Symbol;

@SuppressWarnings(value = { "all" })
%%

%cup
%class ExparserSatScanner
%implements ExparserSatSym
%line
%column

%{  
  private Symbol symbol(String name, int sym, String val) {
    return new Symbol(sym, yyline, yycolumn, val);
  }
  private Symbol symbol(String name, int type) {
    return new Symbol(type, yyline, yycolumn);
  }
  
  private void error(String message) {
    throw new RuntimeException(message + " near " + yyline +":" + yycolumn);
  }
%}
%eofval{
    return symbol("EOF", FeatureModelSym.EOF);
%eofval}

LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n] | .
WhiteSpace     = {LineTerminator} | [ \t\f]

/* comments */
Comment = {TraditionalComment} | {EndOfLineComment}

TraditionalComment   = "/*" [^*] ~"*/" | "/*" "*"+ "/"
EndOfLineComment     = "//" {InputCharacter}* {LineTerminator}

Identifier = [:jletter:] [:jletterdigit:]*

%%

/* keywords */
<YYINITIAL> "not"                { return symbol("not", ExparserSatScanner.NOT); }
<YYINITIAL> "and"                { return symbol("and", ExparserSatScanner.AND); }
<YYINITIAL> "or"                 { return symbol("or", ExparserSatScanner.OR); }

<YYINITIAL> {
  /* identifiers */ 
  {Identifier}                   { return symbol("ID", ExparserSatScanner.IDENTIFIER, yytext()); }
 
  /* literals */

  /* operators */
  "("                           { return symbol("(", ExparserSatScanner.OBRACKETS); }
  ")"                           { return symbol(")", ExparserSatScanner.CBRACKETS); }


  /* comments */
  {Comment}                      { /* ignore */ }
 
  /* whitespace */
  {WhiteSpace}                   { /* ignore */ }
}

<<EOF>> {return symbol("EOF", ExparserSatScanner.EOF); }
/* error fallback */
.|\n                             { error("Illegal character <"+yytext()+">"); }

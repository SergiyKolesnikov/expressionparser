A parser for boolean expressions, generated with CUP and JFlex. 
One version of the parser generates a BDD (JavaBDD), another version loads the expression in the Choco SAT solver

./test.sh
in the main directory calls CUP/JFlex to generate the parsers (both versions), compile them, and execute tests.
#!/bin/bash

# 1. save a BDD with exparser.bdd.tools.BddManager.BDDtoDotFile(BDD, new File("BDD_graph_0.dot"), String)
# 2. generate a pdf representation with this script

for i in 0 1 2 3 4 5 6 7
do
	if test -f BDD_graph_$i.dot
	then 
		dot -Tpdf BDD_graph_$i.dot > graph$i.pdf
		rm BDD_graph_$i.dot
		echo generated $i
	else 
		if test -f graph$i.pdf
		then
			rm graph$i.pdf
			echo deleted graph$i.pdf
		fi
	fi
done

